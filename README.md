### Static Site Generator

To begin work with SSG, run this commands in CMD or terminal in project folder:
```
python start.py & python content_gen.py & python server_run.py
```

**start.py** - generate OUTPUT folder, which contain folder CSS with *main.css* file inside 
and also create empty folder POSTS. This folder will contain parsed HTML pages.

**content_gen.py** - this script will parse .MD files from CONTENT folder and generate
HTML files. Always run this file, when you want to add new HTML page.

**server_run.py** - as is, will run server on *localhost* with 8000 port number.

### Syntax

`H1-H6`

For header text from H1 to H6, use symbol `#` (hash tag) from 1 for `H1` till 6 symbols to `H6`.

`<b></b>`

For **bold text** use double asterisks on `**both sides of text**`.

`<i></i>`

For *italic text* use single asterisk on `*both sides of text*`.
 
`Code Block`

For code block use triple gravis " ``` " on both sides of code.

`Quote block`

For quote block use triple double quotes `"""` on both sides of text.

`Link`

To create a link, `[Insert text between squre brackets](and link between circle brackets)`.

`Images`

Images creates the same way as link, text in square brackets will appear as title when 
hovering image.

### Tags

`<ul> <li> </li> </ul>`

To create numeric list use tag `:list --numeric:`, for pointer list `:list --pointer:`. 
This tag should be written from a new line. When the list will end write `:list:`.

Syntax example:
```
:list --numeric:
First item in list
Second item in list
...
N-th item in list
:list:
```

`Post title`

To specify your post title, you should use tag `:title:`
```
:title: You can't pass this article!
```

`Short post description`

Description must be short, fit in a single line and begin from the tag `:desc:`. 
This description will show under post title at the main page menu
```
:desc: Awersome description of my post
```

`Author`

If you want to insert author's name and display it on top of the page, write it after tag `:author:` in single line
```
:author: John Smith
```

`Date`

The same way you can specify date of publishing by tag `:date:`. Date syntax `day.month.year`
```
:date: 21.01.2019
```
