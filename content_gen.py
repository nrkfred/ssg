from datetime import datetime
import os
import re
from start import HTML, TAGS, file_creator
from jinja2 import Environment, FileSystemLoader
from datetime import date


class StaticSiteGenerator:
    def __init__(self):
        self.files = []
        file_loader = FileSystemLoader('templates')
        env = Environment(loader=file_loader)
        self.template = env.get_template('body.html')
        self.in_file = None
        self.out_file = None
        self.temp_line = ' '
        self.new_line = ' '
        self.line = None
        self.title = 'No title'
        self.description = 'No description'
        self.pub_date = date.today()
        self.author = 'John Smith'
        self.tags = {'"""': False, '**': False, '*': False, '```': False}
        self.list = None
        self.numeric = False
        self.points = False

    def ssg_start(self):
        file_creator()

        contents = os.listdir('content')
        links, titles, descriptions, pub_date, authors = [], [], [], [], []
        for content in contents:
            self.in_file = open(os.path.relpath('content/{}'.format(content)), encoding='utf-8')

            for self.line in self.in_file:
                self.parse()

            self.new_line += self.temp_line

            self.new_line = HTML.format(title=self.title, date=self.pub_date, author=self.author, body=self.new_line)

            with open(os.path.abspath('output/posts/{}.html'.format(content.split('.')[0])), 'w',
                      encoding='utf-8') as out:
                out.write(self.new_line)
            links.append('posts/{}.html'.format(content.split('.')[0]))
            titles.append(self.title)
            descriptions.append(self.description)
            pub_date.append(self.pub_date)
            authors.append(self.author)

            self.new_line, self.temp_line = ' ', ' '
            self.tags = {'"""': False, '**': False, '*': False, '```': False}
            self.title = 'No title'
            self.description = 'No description'
            self.pub_date = date.today()
            self.author = 'John Smith'

        data = self.sort_posts_by_date_desc(links, titles, descriptions, pub_date, authors)

        html = self.template.render(data=data)
        # html = template.render(data=data)
        with open('output/index.html', 'w', encoding='utf-8') as f:
            f.write(html)

    def parse(self):
        self.line = self.line.strip('\n')
        if self.line in ('\n', ''):
            self.temp_line += TAGS['p'].format(line='') + ' '
            return

        s = self.line.find('#')
        e = self.line.rfind('#')
        if s == 0:
            size = e + 1
            self.temp_line += TAGS['h'].format(line=self.line[size:], size=size) + ' '
            return

        if re.match(r'^(---)$', self.line):
            self.new_line += TAGS['hr']
            return

        if re.match(r'(^:desc:)', self.line):
            self.description = self.line[6:].strip()
            return

        if re.match(r'(^:title:)', self.line):
            self.title = self.line[7:].strip()
            return

        if re.match(r'(^:date:)', self.line):
            self.pub_date = self.line[6:].strip()
            return

        if re.match(r'(^:author:)', self.line):
            self.author = self.line[8:].strip()
            return

        if re.match(r'(:list --numeric:)', self.line):
            self.temp_line += TAGS['list_numeric_begin']
            self.numeric = True
            return

        if re.match(r'(:list --points)', self.line):
            self.temp_line += TAGS['list_points_begin']
            self.points = True
            return

        if re.match(r'(:list:)', self.line):
            self.temp_line += TAGS['list_numeric_end'] if self.numeric else TAGS['list_points_end']
            self.points = False
            self.numeric = False
            return

        if self.numeric or self.points:
            pattern = re.compile(r'(\d\.\s)+?')
            self.line = pattern.sub('', self.line)
            self.temp_line += TAGS['list'].format(line=self.line)
            return

        if re.match(
                r'[\w\d\s:/.?=&-_#]*?\[(.)+\]\((http)?s?:?(//[^"\'#]*((?:png|jpg|jpeg|gif|svg)))\)[\w\d\s:/.?=&-_#]*?',
                self.line):
            pattern = re.compile(r'\[(.)+\]\([\w\d:/.?=&-_#]+\)')
            src = re.search(r'\([\w\d:/.?=&-_#]+\)', self.line).group(0)
            title = re.search(r'\[(.)+\]\s?', self.line).group(0)
            link = TAGS['img'].format(src=src[1:-1], title=title[1:-1])
            self.line = pattern.sub(link, self.line)

        if re.match(r'[\w\d\s:/.?=&-_#]*?\[(.)+\]\([\w\d:/.?=&-_#]+\)[\w\d\s:/.?=&-_#]*?', self.line):
            pattern = re.compile(r'\[(.)+\]\([\w\d:/.?=&-_#]+\)')
            href = re.search(r'\([\w\d:/.?=&-_#]+\)', self.line).group(0)
            title = re.search(r'\[(.)+\]', self.line).group(0)
            link = TAGS['href'].format(href=href[1:-1], title=title[1:-1])
            self.line = pattern.sub(link, self.line)

        self.search()

    def search(self):
        words = self.line.split(' ')

        for word in words:
            flag = False
            for elem in self.tags:
                if word.rfind(elem) - word.find(elem) > 0:
                    self.temp_line += word + ' '
                    self.tags[elem] = False
                    flag = False

                    s = self.temp_line.find(elem)
                    e = self.temp_line.rfind(elem)
                    elem_len = len(elem)

                    self.new_line += self.temp_line[0:s] + TAGS[elem].format(line=self.temp_line[s + elem_len:e]) + \
                                     ' ' + self.temp_line[e + elem_len:]
                    self.temp_line = ' '
                    break

                if word.find(elem) >= 0 and not self.tags[elem]:
                    self.temp_line += word + ' '
                    self.tags[elem] = True
                    flag = False
                    break
                elif word.find(elem) == -1:
                    flag = True
                elif word.find(elem) >= 0 and self.tags[elem]:
                    self.temp_line += word + ' '
                    self.tags[elem] = False
                    flag = False

                    s = self.temp_line.find(elem)
                    e = self.temp_line.rfind(elem)
                    elem_len = len(elem)

                    self.new_line += self.temp_line[0:s] + TAGS[elem].format(line=self.temp_line[s + elem_len:e]) + \
                                     ' ' + self.temp_line[e + elem_len:]
                    self.temp_line = ' '
                    break

            if flag:
                self.temp_line += word + ' '

    def sort_posts_by_date_desc(self, links, titles, descriptions, pub_date, authors):
        pub_date_df = [datetime.strptime(x, '%d.%m.%Y') for x in pub_date]  # publication date in date format
        pub_date_df.sort(reverse=True)
        pub_date_sf = ["{:%d.%m.%Y}".format(x) for x in pub_date_df]  # publication date in string format
        indexes = [pub_date.index(pub_date_sf[x]) + 1 for x in range(len(pub_date_sf))]  # indexes of order
        data = sorted(zip(indexes, links, titles, descriptions, pub_date, authors))
        return data


StaticSiteGenerator().ssg_start()
