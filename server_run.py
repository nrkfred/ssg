import http.server
import socketserver
import os

PORT = 8000
pwd = os.getcwd()

Handler = http.server.SimpleHTTPRequestHandler

try:
    os.chdir('output')
    with socketserver.TCPServer(("", PORT), Handler) as httpd:
        print('To stop server, press CTRL+C (CMD+C)')
        print('Server start at port:', PORT)
        httpd.serve_forever()
except KeyboardInterrupt:
    pass
finally:
    os.chdir(pwd)
