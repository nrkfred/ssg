# The 7 Most In-Demand Programming Languages of 2018

:title: The 7 Most In-Demand Programming Languages of 2018
:desc: By Coding Dojo
:author: Coding Dojo
:date: 13.12.2017

Software development is a dynamic field. New and in-demand programming languages, frameworks and technologies can 
emerge, rise to fame, and then fade away in the course of a few years. Developers need to constantly be learning new 
skills to stay relevant. At Coding Dojo, we’re continually evaluating which programming languages are in high demand 
from employers so we can prepare our students to enter the job market. There are many ways to measure a programming 
language’s popularity, but we believe examining job demand is most useful because it shows developers the skills to 
learn to improve their career prospects. 

To accomplish that, we analyzed data from job website Indeed.com on twenty-five programming languages, stacks and 
frameworks to determine the top seven most in-demand coding languages as we move into 2018. This analysis is based on 
the number of job postings for each language. Some languages like Swift and Ruby didn’t make the top seven because they 
have lower job demand, even though developers love them. You can read the results of similar analysis from 2016 and 
2017 on our blog.

Here’s our list, in order from most to least in-demand.

[November, 17th 2017](http://www.codingdojo.com/blog/wp-content/uploads/2018-developer-job-listings-for-most-popular-programming-languages1.jpg)

##1. Java

Java decreased in popularity by about 6,000 job postings in 2018 compared to 2017, but is still extremely 
well-established. Java is over 20 years old, used by millions of developers and billions of devices worldwide, 
and able to run on any hardware and operating system through the Java Virtual Machine. All Android apps are based 
on Java and 90 percent of Fortune 500 companies use Java as a server-side language for backend development. Java 
[Enterprise Edition 8](http://www.oracle.com/technetwork/java/javaee/overview/index.html) and 
[Java 9](http://jdk.java.net/9/) both launched in September 2017 as the Eclipse Foundation 
[took over managing](https://www.infoworld.com/article/3224450/java/unwanted-by-oracle-java-ee-gets-adopted-by-eclipse.html#tk.drr_mlt) 
Java EE from Oracle.

##2. Python

Python grew in popularity by about 5,000 job postings over 2017. It is a general-purpose programming language used for 
web development and as a support language for software developers. It’s also widely used in scientific computing, 
[data mining](http://stackoverflow.com/research/developer-survey-2016#most-popular-technologies-per-occupation) and 
machine learning. The continued growth and demand for machine learning developers may be driving the popularity of Python.

##3. JavaScript

JavaScript, the grandfather of programming languages, is roughly as popular today as it was in our last blog post. 
That’s no surprise to us – JavaScript is used by over [80% of developers](https://insights.stackoverflow.com/survey/2017#technologies-and-occupations) 
and by [95% of all websites](https://w3techs.com/technologies/details/cp-javascript/all/all) 
for any dynamic logic on their pages. Several front-end frameworks for JavaScript such as React and AngularJS have huge future potential 
as IoT and mobile devices become more popular, so we doubt we’ll see JavaScript drop in popularity anytime soon.

##4. C++

C++ changed very little in popularity from early 2017 to now. An extension of the old-school “C” programming language, 
C++ is usually used for system/application software, game development, drivers, client-server applications and embedded 
firmware. Many programmers find C++ complex and more difficult to learn and use than languages like Python or JavaScript, 
but it remains in use in many legacy systems at large enterprises.

##5. C sharp

C# (pronounced “C sharp”) went down slightly in demand this year. C# is an object-oriented programming language from 
Microsoft designed to run on Microsoft’s .NET platform and to make development quicker and easier than Microsoft’s 
previous languages. [C# 7.2](https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-7-2) came out in November, 
adding several new features geared towards avoiding unnecessary copying. C#, like C++, is heavily used in video game 
development, so any aspiring video game developers would do well to learn both of them.

##6. PHP
PHP, a scripting language used on the server side, moved up to number six in our ranking over number nine last year. 
Most developers use PHP for web development, either to add functions that HTML can’t handle or to interact with MySQL databases.

##7. Perl

Perl dropped by about 3,000 job postings and stayed in seventh place in our analysis. Perl 5 and Perl 6 are both chugging 
along and Perl continues to be popular for system and network administrators and as a glue language.