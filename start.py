import os

HTML = """
<!DOCTYPE html>
<html lang="en">
    <meta charset="UTF-8">
    <title>{title}</title>
    <link href="../css/main.css" rel="stylesheet">
<body>
<div class="container">
    <div class="content">
        <a href="/">Main page</a>
        <hr style="margin-top: 5px; margin-bottom: 0; border: 0.5px solid rgba(0, 0, 255, 0.05);">
        <br>
        <div class="info">
            <date>Publishing date: {date}</date>
            <author>Author: {author}</author>
        </div>
        {body}
    </div>
</div>
</body>
</html>
"""

TAGS = {
    'h': '<hr /><h{size}>{line}</h{size}><hr />',
    '**': '<b>{line}</b>',
    '*': '<i>{line}</i>',
    'href': '<a href="{href}">{title}</a>',
    'img': '<a href="{src}"><img src="{src}" title="{title}"></a>',
    '```': '<div style="position: relative;display: block;background-color: #aaa;padding: 5px; width: max-content;'
           'border-radius: 3px; width: 100%;"><code>{line}</code></div>',
    'br': '<br />',
    'p': '<p>{line}</p>',
    'sp': '<p>',
    'ep': '</p>',
    '"""': '<quote style="position: relative;display: block;background-color: #eee;padding: 5px;width: max-content;'
           'border-radius: 3px; width: 100%;">&ldquo;{line}&rdquo;</quote>',
    'list_numeric_begin': '<ol>',
    'list_numeric_end': '</ol>',
    'list_points_begin': '<ul>',
    'list_points_end': '</ul>',
    'list': '<li>{line}</li>',
    'hr': '<hr />',
}


def file_creator():
    style = """
body {
    background-color: rgba(0, 0, 255, 0.03);
    font-family: "Lucida Grande", Verdana, Arial, sans-serif;
    font-size: 15px;
}

.container {
    position: relative;
    width: 800px;
    height: auto;
    display: block;
    margin: 0 auto;
    background-color: rgba(0, 0, 255, 0.2);
    box-shadow: 0 0 10px rgba(0, 0, 255, 0.5);
    border-radius: 5px;
    padding: 15px;
}

.content {
    position: relative;
    display: block;
    width: auto;
    height: auto;
    padding: 10px;
}

a {
    text-decoration: none;
    color: rgba(0, 0, 255, 0.8);
}

a:hover {
    color: #000;
}

a:active {
    color: rgba(0, 0, 255, 0.8);
}

hr {
    border: .5px solid rgba(0, 0, 255, 0.1);
}

.description li {
    list-style-type: none;
    color: #444;
}

.info {
    width: 100%;
    height: 10px;
    position: relative;
}

date {
    font-size: 12px;
    float: left;
}

author {
    font-size: 12px;
    float: right;
}

img {
    max-width: 160px;
    max-height: inherit;
}

span {
    font-size: 14px;
}
    """

    if not os.path.exists(os.path.abspath('output/css')):
        os.makedirs(os.path.abspath('output/css'))
        with open(os.path.abspath('output/css/main.css'), 'w', encoding='utf-8') as f:
            f.write(style)

    if not os.path.exists(os.path.abspath('output/posts')):
        os.makedirs(os.path.abspath('output/posts'))


file_creator()
